# Note
`containerPort` support only default port 80

<br> 

# To run nginx kubernetes
1. `$ minikube start`
1. `$ kubectl apply -f nginx-deployment.yaml`
1. `$ kubectl apply -f nginx-external-service.yaml`
1. `$ minikube service external-nginx-service` to access from outside